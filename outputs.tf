output "name" {
  description = "The name of the provisioned queue."
  value       = aws_sqs_queue.default.name
}

output "consumer_policy_arn" {
  description = "The ARN of an IAM policy granting permission to receive messages from this queue."
  value       = aws_iam_policy.consumer.arn
}

output "producer_policy_arn" {
  description = "The ARN of an IAM policy granting permission to send messages to this queue."
  value       = aws_iam_policy.producer.arn
}
