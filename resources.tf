resource "aws_kms_key" "queue" {
  key_usage                = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  is_enabled               = true
  enable_key_rotation      = false

  tags = merge(var.tags, {
    Name = "${var.name}-queue"
  })
}

resource "aws_sqs_queue" "default" {
  name_prefix                       = "${var.name}-"
  visibility_timeout_seconds        = 30
  message_retention_seconds         = 345600
  max_message_size                  = 262144
  delay_seconds                     = 0
  receive_wait_time_seconds         = 20
  fifo_queue                        = true
  content_based_deduplication       = true
  kms_master_key_id                 = aws_kms_key.queue.arn
  kms_data_key_reuse_period_seconds = 300

  tags = merge(var.tags, {
    Name = var.name
  })
}

resource "aws_iam_policy" "consumer" {
  name_prefix = "${var.name}-queue-consumer-"
  path        = "/"
  policy      = data.aws_iam_policy_document.consumer.json

  tags = merge(var.tags, {
    Name = "${var.name}-queue-consumer"
  })
}

resource "aws_iam_policy" "producer" {
  name_prefix = "${var.name}-queue-producer-"
  path        = "/"
  policy      = data.aws_iam_policy_document.producer.json

  tags = merge(var.tags, {
    Name = "${var.name}-queue-producer"
  })
}
