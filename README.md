# SQS Queue | AWS | Terraform Modules | Twuni

Terraform module for provisioning an encrypted FIFO AWS SQS queue
with a dedicated KMS key and IAM policy for consumers and producers
to use.

This module applies opinionated yet reasonable defaults. If any
of the defaults are not to your liking, fork the repo and tweak as
needed. The module is simple enough that doing so should not create
a significant operational burden.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.52.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_name"></a> [name](#input\_name) | The base name for the SQS queue to be provisioned. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_consumer_policy_arn"></a> [consumer\_policy\_arn](#output\_consumer\_policy\_arn) | The ARN of an IAM policy granting permission to receive messages from this queue. |
| <a name="output_name"></a> [name](#output\_name) | The name of the provisioned queue. |
| <a name="output_producer_policy_arn"></a> [producer\_policy\_arn](#output\_producer\_policy\_arn) | The ARN of an IAM policy granting permission to send messages to this queue. |
