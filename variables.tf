variable "name" {
  description = "The base name for the SQS queue to be provisioned."
  type        = string
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
